<?php

/*

Author: Eddie Machado

URL: htp://themble.com/bones/



This is where you can drop your custom functions or

just edit things like thumbnail sizes, header images,

sidebars, comments, ect.

*/

require_once( 'payment-gateway-settings/payment-gateway-settings.php' );




// LOAD BONES CORE (if you remove this, the theme will break)

require_once( 'library/bones.php' );



// USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY

// require_once( 'library/custom-post-type.php' );



// CUSTOMIZE THE WORDPRESS ADMIN (off by default)

// require_once( 'library/admin.php' );



// Load the theme specific files

include get_template_directory() . '/library/panels.php';



/*********************

LAUNCH BONES

Let's get everything up and running.

*********************/



function bones_ahoy() {



  // let's get language support going, if you need it

	load_theme_textdomain( 'stroschtheme', get_template_directory() . '/languages' );



  // launching operation cleanup

	add_action( 'init', 'bones_init' );

  // A better title

	add_filter( 'wp_title', 'rw_title', 10, 3 );

  // remove WP version from RSS

	add_filter( 'the_generator', 'bones_rss_version' );

  // remove pesky injected css for recent comments widget

	add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );

  // clean up comment styles in the head

	add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );

  // clean up gallery output in wp

	add_filter( 'gallery_style', 'bones_gallery_style' );



	add_filter('show_admin_bar', '__return_false');



  // Call shortcodes in text widgets (test123)

	add_filter('widget_text', 'do_shortcode');



  // enqueue base scripts and styles

	add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );



  // launching this stuff after theme setup

	bones_theme_support();



  // adding sidebars to Wordpress (these are created in functions.php)

	add_action( 'widgets_init', 'bones_register_sidebars' );



  // cleaning up random code around images

	add_filter( 'the_content', 'bones_filter_ptags_on_images' );

  // cleaning up excerpt

	add_filter( 'excerpt_more', 'bones_excerpt_more' );



} /* end bones ahoy */



// let's get this party started

add_action( 'after_setup_theme', 'bones_ahoy' );





/************* OEMBED SIZE OPTIONS *************/



if ( ! isset( $content_width ) ) {

	$content_width = 1000;

}



/************* THUMBNAIL SIZE OPTIONS *************/



// Thumbnail sizes

add_image_size( 'bones-thumb-600', 600, 150, true );

add_image_size( 'bones-thumb-300', 300, 100, true );

add_image_size( 'popular-product', 340, 260, false );

add_image_size( 'tile', 200, 120, false );



/*

to add more sizes, simply copy a line from above

and change the dimensions & name. As long as you

upload a "featured image" as large as the biggest

set width or height, all the other sizes will be

auto-cropped.



To call a different size, simply change the text

inside the thumbnail function.



For example, to call the 300 x 300 sized image,

we would use the function:

<?php the_post_thumbnail( 'bones-thumb-300' ); ?>

for the 600 x 100 image:

<?php the_post_thumbnail( 'bones-thumb-600' ); ?>



You can change the names and dimensions to whatever

you like. Enjoy!

*/



add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );



function bones_custom_image_sizes( $sizes ) {

	return array_merge( $sizes, array(

		'bones-thumb-600' => __('600px by 150px'),

		'bones-thumb-300' => __('300px by 100px'),

		'popular-product' => __('Popular Product'),

'popular-product' => __('Test'),

		'tile' => __('Tile (200x120)')

		) );

}



/*

The function above adds the ability to use the dropdown menu to select

the new images sizes you have just created from within the media manager

when you add media to your content blocks. If you add more image sizes,

duplicate one of the lines in the array and name it according to your

new image size.

*/



/************* ACTIVE SIDEBARS ********************/



// Sidebars & Widgetizes Areas

function bones_register_sidebars() {

	register_sidebar(array(

		'id' => 'sidebar1',

		'name' => __( 'Sidebar left', 'bonestheme' ),

		'description' => __( 'Left sidebar.', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));



	register_sidebar(array(

		'id' => 'sidebar2',

		'name' => __( 'Sidebar right', 'bonestheme' ),

		'description' => __( 'Right sidebar.', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));



	register_sidebar(array(

		'id' => 'footer',

		'name' => __( 'Footer', 'bonestheme' ),

		'description' => __( 'The footer widget.', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s">',

		'after_widget' => '</div>',

		// 'before_title' => '<h4 class="widgettitle">',

		// 'after_title' => '</h4>',

		));



	register_sidebar(array(

		'id' => 'sidebar_banner_left_sidebar',

		'name' => __( 'Banners Left Sidebar', 'bonestheme' ),

		'description' => __( 'The area for banners of the left-hand side of the page showing up under the sidebar.', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s banners-left-sidebar">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));



	register_sidebar(array(

		'id' => 'sidebar_banner_right',

		'name' => __( 'Banners Right', 'bonestheme' ),

		'description' => __( 'The area for banners of the right-hand side of the page.', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s banners-right">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));



	register_sidebar(array(

		'id' => 'partnerstorecode',

		'name' => __( 'Partner Store Code - Left', 'bonestheme' ),

		'description' => __( 'Here goes the HTML Code of the Partner Store', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s amazonwidget partnerstorecode">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));

		

	register_sidebar(array(

		'id' => 'partnerstorecoderight',

		'name' => __( 'Partner Store Code - Right', 'bonestheme' ),

		'description' => __( 'Here goes the HTML Code of the Partner Store', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s amazonwidget partnerstorecode">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));





	register_sidebar(array(

		'id' => 'sidebar-shop',

		'name' => __( 'Sidebar - Shop', 'bonestheme' ),

		'description' => __( 'Sidebar for shop links', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));

        	register_sidebar(array(

		'id' => 'sidebar-blog',

		'name' => __( 'Sidebar - Blog', 'bonestheme' ),

		'description' => __( 'Sidebar for blog links', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));



	register_sidebar(array(

		'id' => 'sidebar-bookshop',

		'name' => __( 'Sidebar - Bookshop', 'bonestheme' ),

		'description' => __( 'Sidebar for bookshop links', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

		));

	

	/*

	to add more sidebars or widgetized areas, just copy

	and edit the above sidebar code. In order to call

	your new sidebar just use the following code:



	Just change the name to whatever your new

	sidebar's id is, for example:



	register_sidebar(array(

		'id' => 'sidebar2',

		'name' => __( 'Sidebar 2', 'bonestheme' ),

		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),

		'before_widget' => '<div id="%1$s" class="widget %2$s">',

		'after_widget' => '</div>',

		'before_title' => '<h4 class="widgettitle">',

		'after_title' => '</h4>',

	));



	To call the sidebar in your template, you can just copy

	the sidebar.php file and rename it to your sidebar's name.

	So using the above example, it would be:

	sidebar-sidebar2.php



	*/

} // don't remove this bracket!





/************* COMMENT LAYOUT *********************/



// Comment Layout

function bones_comments( $comment, $args, $depth ) {

	$GLOBALS['comment'] = $comment; ?>

	<div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>

		<article  class="cf">

			<header class="comment-author vcard">

				<?php

        /*

          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:

          echo get_avatar($comment,$size='32',$default='<path_to_url>' );

        */

          ?>

          <?php // custom gravatar call ?>

          <?php

          // create variable

          $bgauthemail = get_comment_author_email();

          ?>

          <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />

          <?php // end custom gravatar call ?>

          <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'bonestheme' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ) ?>

          <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>



      </header>

      <?php if ($comment->comment_approved == '0') : ?>

      	<div class="alert alert-info">

      		<p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>

      	</div>

      <?php endif; ?>

      <section class="comment_content cf">

      	<?php comment_text() ?>

      </section>

      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>

  </article>

  <?php // </li> is added by WordPress automatically ?>

  <?php

} // don't remove this bracket!





/*

This is a modification of a function found in the

twentythirteen theme where we can declare some

external fonts. If you're using Google Fonts, you

can replace these fonts, change it in your scss files

and be up and running in seconds.

*/

function custom_css() {

	wp_enqueue_style('font_1', '//fonts.googleapis.com/css?family=Lato:400,700|Roboto+Slab:700|Open+Sans:400italic,400,700,600,300|Damion');
	wp_enqueue_style('flaticon', get_template_directory_uri() .  '/library/fonts/font/flaticon.css');
	wp_enqueue_style('icomoon', get_template_directory_uri() . '/library/fonts/icomoon/style.css');
	wp_enqueue_script('foundation', get_template_directory_uri() . '/library/css/search-box/foundation.js');

}

add_action('wp_enqueue_scripts', 'custom_css');







// STROSCH

function register_my_menu() {

	register_nav_menu('top-menu',__( 'Top Menu' ));

	register_nav_menu('mobile-nav',__( 'Mobile Menu' ));

}

add_action( 'init', 'register_my_menu' );



// filter the tab display value to conditionally turn off the tab

function rum_filter_simple_side_tab_display( $display ) {



    if ( ! is_front_page() ) {



        $display = false;

    }



    return $display;

}

add_filter( 'rum_sst_plugin_display_tab', 'rum_filter_simple_side_tab_display', 10 , 1 );





add_filter( 'cron_schedules', 'cron_add_day' );

function cron_add_day( $schedules ) {

	$schedules['day'] = array(

		'interval' => 60 * 60 * 24,

		'display' => 'Per day'

	);

	return $schedules;

}





add_action('wp', 'my_activation');

function my_activation() {

	if ( ! wp_next_scheduled( 'my_day_event' ) ) {

		wp_schedule_event( time(), 'day', 'my_day_event');

	}

}



add_action('my_day_event', 'do_every_day');

function do_every_day() {

	global $wpdb;

	$db_sbm_banners = $wpdb->prefix . 'sbm_banners';

	$change = $wpdb->get_results(

			"

		   	SELECT   	status, expires, id

            FROM        $db_sbm_banners

			"

		);

	foreach ($change as $key) {

		$date = strtotime($key->expires);

		$id = $key->id;

		$current_date = time();

		if($current_date > $date){

			$wpdb->query(

				"

				UPDATE $db_sbm_banners

				SET `status` = 'disable'

				WHERE `id` = $id

				"

			);

			//echo $key->status."-".$date."-".$id."-".$current_date."<br>";

		}

		

	}

}







require_once( 'widgets/widgets.php' );





///Blog

add_image_size( 'blog-thumb', 320, 190, true );







add_action( 'wp_ajax_blog_mail_send_action', 'blog_mail_send_action' );

add_action( 'wp_ajax_nopriv_blog_mail_send_action', 'blog_mail_send_action' );

function blog_mail_send_action(){

	$cname = $_POST['cname'];

	$cemail = $_POST['cemail'];

	$phone = $_POST['phone'];

	$ccomment = $_POST['ccomment'];

	

	$hidden_title = $_POST['hidden_title'];

	$hidden_mail = $_POST['hidden_mail'];

	

	$to = $hidden_mail;

	$subject = $hidden_title;

	

	$message = "

	<html>

	<head>

	<title>$hidden_title</title>

	</head>

	<body>

	<p><strong>$hidden_title</strong></p>

	<table>

	

	<tr>

	<td><strong>Name : </strong></td>

	<td>$cname</td>

	</tr>

	

    <tr>

	<td><strong>Phone : </strong></td>

	<td>$phone</td>

	</tr>

	

    <tr>

	<td><strong>Comment : </strong></td>

	<td>$ccomment</td>

	</tr>

	

	

	</table>

	</body>

	</html>

	";

	

	// Always set content-type when sending HTML email

	$headers = "MIME-Version: 1.0" . "\r\n";

	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	

	// More headers

	$headers .= 'From: <'.$cemail.'>' . "\r\n";

	

	if(mail($to,$subject,$message,$headers)){

	 echo 'Success';

	}else{

	 echo 'Faild';

	}



die();

}



add_action( 'show_user_profile', 'blog_extra_user_profile_fields' );

add_action( 'edit_user_profile', 'blog_extra_user_profile_fields' );

function blog_extra_user_profile_fields( $user ) {

$payment_completed = esc_attr( get_the_author_meta( '_payment_completed', $user->ID ) );

?>

  <h3><?php _e("Blog add Permission", "blank"); ?></h3>

  <table class="form-table">

    <tr>

      <th><label for="payment_completed"><?php _e("You can post blog"); ?></label></th>

      <td>

        <select name="_payment_completed" id="payment_completed">

          <option value="0" <?php if($payment_completed==0 || $payment_completed==''){?>selected="selected"<?php } ?>><font><font>No</font></font></option>

          <option value="1" <?php if($payment_completed==1){?>selected="selected"<?php } ?>><font><font>Yes</font></font></option>

        </select>

    </td>

    </tr>

  </table>

<?php

}



add_action( 'personal_options_update', 'blog_save_extra_user_profile_fields' );

add_action( 'edit_user_profile_update', 'blog_save_extra_user_profile_fields' );

function blog_save_extra_user_profile_fields( $user_id ) {

  $saved = false;

  if ( current_user_can( 'edit_user', $user_id ) ) {

    update_user_meta( $user_id, '_payment_completed', $_POST['_payment_completed'] );

    $saved = true;

  }

  return true;

}





add_action( 'wp_ajax_advance_search_header', 'advance_search_header' );

add_action( 'wp_ajax_nopriv_advance_search_header', 'advance_search_header' );

function advance_search_header(){

  $counter = '';
  
  global $switched;
  switch_to_blog(1);
  if($_POST['category']=='all'){



	$get_SearchIndex = 'All'; 

    $get_keywords =  $_POST['keyword'];
   

  }else{	

    $get_SearchIndex = get_post_meta($_POST['category'], 'Kategorie', true);

    $get_keywords = get_post_meta($_POST['category'], 'Suchwort', true);

  }
  restore_current_blog(); 

 // $Keywords = urlencode($get_keywords);
  $Keywords = $get_keywords;

  $SearchIndex = $get_SearchIndex;

  $ItemPage = 3;		

	

	

	

  $version = '2011-08-01';

 

  $params['Operation'] = 'ItemSearch';

  $params['Keywords'] = $Keywords;

  $params['SearchIndex'] = $SearchIndex;

  //$params['ItemPage'] = $ItemPage;

  $params['ResponseGroup'] = 'Medium,Offers,Variations';

  

  /////////////////////////////////

   

    // Amazon de

    $region = 'de';

	$method = 'GET';

    $host = 'webservices.amazon.'.$region;

    $uri = '/onca/xml';



	//  origin amazon product api credentials



	$public_key = 'AKIAIBQSDIXISC4GIBIQ';

	$private_key = 'aB/21FthqYG4W4e3FOd9sCBP/ZhOHfERyA3Q69xj';

	$associate_tag = 'wwwhochzeitse-21';

    

   

    $params['Service'] = 'AWSECommerceService';

    $params['AWSAccessKeyId'] = $public_key;

   

    $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');

   

    $params['Version'] = $version;

    if ($associate_tag !== NULL) {

        $params['AssociateTag'] = $associate_tag;

    }

    

  

  

    for($i=1; $i<=$ItemPage; $i++){

		

	$params['ItemPage'] = $i;

	

    ksort($params);

  

    $canonicalized_query = array();



    foreach ($params as $param=>$value)

    {

        $param = str_replace('%7E', '~', rawurlencode($param));

        $value = str_replace('%7E', '~', rawurlencode($value));

        $canonicalized_query[] = $param.'='.$value;

    }

    $canonicalized_query = implode('&', $canonicalized_query);

    

    

    $string_to_sign = $method."\n".$host."\n".$uri."\n".$canonicalized_query;

    

   

    $signature = base64_encode(hash_hmac('sha256', $string_to_sign, $private_key, TRUE));

    

   

    $signature = str_replace('%7E', '~', rawurlencode($signature));

    

  

    $request = 'http://'.$host.$uri.'?'.$canonicalized_query.'&Signature='.$signature;

    

	

    ////////////////////////////////////



	

	$session = curl_init($request);

	curl_setopt($session, CURLOPT_HEADER, false);

	curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

	$response = curl_exec($session);

	curl_close($session); 

		

	$parsed_xml = simplexml_load_string($response);

	

	

	///////////////////////////////////////////////////////

	

     $numOfItems = $parsed_xml->Items->TotalResults;

     $totalPages = $parsed_xml->Items->TotalPages;

 	

	if($numOfItems>0){

		foreach($parsed_xml->Items->Item as $current){

			

			if(isset($current->SmallImage->URL) && ($current->Offers->Offer->OfferListing->Price->FormattedPrice)){

				$title = $current->ItemAttributes->Title;

			    $search_keyword = $_POST['keyword'];
                if($_POST['category']!='all'){
				 if(!(stripos($title,$search_keyword) === false )){

				 $counter = 1;	

				 

				 //$link = 'http://hochzeit-selber-planen.com/product-details/?id='.$current->ASIN;

				 ?>

                

				 <li>

                    <div class="thumb-box"><a href="<?php echo $current->DetailPageURL; ?>" target="_blank"><img src="<?php echo $current->SmallImage->URL; ?>" width="70px" height="70px"></a></div>

                    <h4><a href="<?php echo $current->DetailPageURL; ?>" target="_blank"><?php echo $current->ItemAttributes->Title; ?></a></h4>

                    <div class="price-box"><a href="<?php echo $current->DetailPageURL; ?>" target="_blank"><?php echo $current->Offers->Offer->OfferListing->Price->FormattedPrice; ?></a></div>

                 </li>

				

			<?php 

			 }
			    }else{
				  //if(strpos($title,$search_keyword) ){

				 $counter = 1;	

				 

				 //$link = 'http://hochzeit-selber-planen.com/product-details/?id='.$current->ASIN;

				 ?>

                

				 <li>

                    <div class="thumb-box"><a href="<?php echo $current->DetailPageURL; ?>" target="_blank"><img src="<?php echo $current->SmallImage->URL; ?>" width="70px" height="70px"></a></div>

                    <h4><a href="<?php echo $current->DetailPageURL; ?>" target="_blank"><?php echo $current->ItemAttributes->Title; ?></a></h4>

                    <div class="price-box"><a href="<?php echo $current->DetailPageURL; ?>" target="_blank"><?php echo $current->Offers->Offer->OfferListing->Price->FormattedPrice; ?></a></div>

                 </li>

				

			<?php 

			 //}
			 }

			}



		}

	}

	

	

	}

	

	if($counter==''){

	echo '<li>Keine Ergebnisse</li>';

	}







 die();

}

////////////////////////////////////////////////////Page Ratgeber Meta Box Start///////////////////////////////////////////////////////
function page_get_custom_field( $value ) {
	global $post;

    $custom_field = get_post_meta( $post->ID, $value, true );
    if ( !empty( $custom_field ) )
	    return is_array( $custom_field ) ? stripslashes_deep( $custom_field ) : stripslashes( wp_kses_decode_entities( $custom_field ) );
        return false;
}

function page_add_custom_meta_box() {
	add_meta_box( 'page-meta-box', __( 'Is ratgeber page?', 'page' ), 'page_meta_box_output', 'page', 'normal', 'default' );
}
add_action( 'add_meta_boxes', 'page_add_custom_meta_box' );

function page_meta_box_output( $post ) {
	// create a nonce field
	wp_nonce_field( 'my_page_meta_box_nonce', 'page_meta_box_nonce' ); ?>
	<p>
		<input type="checkbox" name="_is_ratgeber" value="1" <?php if(page_get_custom_field( '_is_ratgeber' )==1){?>checked="checked"<?php }?>><strong><?php _e( ' Ratgeber', 'page' ); ?></strong>
    </p>
	<?php
}

/**
 * Save the Meta box values
 */
function page_meta_box_save( $post_id ) {
	// Stop the script when doing autosave
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

	// Verify the nonce. If insn't there, stop the script
	if( !isset( $_POST['page_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['page_meta_box_nonce'], 'my_page_meta_box_nonce' ) ) return;

	// Stop the script if the user does not have edit permissions
	if( !current_user_can( 'edit_post', get_the_id() ) ) return;

    // Save the textfield
	  if($_POST['_is_ratgeber']==1){
		update_post_meta( $post_id, '_is_ratgeber', esc_attr( $_POST['_is_ratgeber'] ) );
	  }else{
	    update_post_meta( $post_id, '_is_ratgeber','');
	  }
				
}
add_action( 'save_post', 'page_meta_box_save' );
////////////////////////////////////////////////////Page Ratgeber Meta Box End///////////////////////////////////////////////////////

function js_defer_attr($tag){
	# Do not add async to these scripts
	$scripts_to_exclude = array('jquery.js', 'datepicker.min.js');
	 
	foreach($scripts_to_exclude as $exclude_script){
		if(true == strpos($tag, $exclude_script ) )
		return $tag;	
	}

	# Add async to all remaining scripts
	return str_replace( ' src', ' defer="defer" src', $tag );
}
if ( ! is_admin() ) {
	//add_filter( 'script_loader_tag', 'js_defer_attr', 10 );
}

function remove_location() {
	//unset($_COOKIE['awpcp-regions-current-location']);
	//unset($_COOKIE['awpcp-using-session-cookies']);
	
	$request_handler = awpcp_set_location_request_handler();
	$request_handler->clear_location();
	
	//$regions = AWPCP_Region_Control_Module::instance();
	//$regions->set_location(null);
}

//add_action( 'init', 'remove_location' );

wp_enqueue_style( 'easy-social-share-buttons-essb-followers-counter',get_template_directory_uri().'/css/easy-social-share-buttons-essb-followers-counter.min.css');
wp_enqueue_style( 'easy-social-share-buttons-styles',get_template_directory_uri().'/css/easy-social-share-buttons-styles.css');

wp_enqueue_script('my-length-script', get_template_directory_uri() .'/js/length_optimize.js', array('jquery'), null, true);

/* DON'T DELETE THIS CLOSING TAG */ ?>
