<?php
function pagebuilder_panels_row_styles($styles) {
	// Default == boxed container centered
	$styles['wide-bg'] = __('Wide Background');
	$styles['wide-content'] = __('Wide Content');
	return $styles;
}
add_filter('siteorigin_panels_row_styles', 'pagebuilder_panels_row_styles');

/*STROSCH-CUSTOM-CODE*/
/*
function pagebuilder_panels_row_classes($fields) {
	$fields['class2'] = array(
		'name' => __('Custom Classes'),
		'type' => 'text',
	);
	return $fields;
}
add_filter('siteorigin_panels_row_classes', 'pagebuilder_panels_row_classes');
*/

function pagebuilder_panels_row_style_fields($fields) {
	/*STROSCH-CUSTOM-CODE*/
	$fields['customclasses'] = array(
		'name' => __('Custom Classes'),
		'type' => 'text',
	);
	$fields['padding'] = array(
		'name' => __('Padding'),
		'type' => 'text',
	);
	$fields['margin-top'] = array(
		'name' => __('Margin-Top'),
		'type' => 'text',
	);
	$fields['margin-bottom'] = array(
		'name' => __('Margin-Bottom'),
		'type' => 'text',
	);
	$fields['background'] = array(
		'name' => __('Background'),
		'type' => 'text',
	);
	$fields['background-size'] = array(
		'name' => __('Background-Size<br>"contain", "cover"'),
		'type' => 'text',
	);
	$fields['border'] = array(
		'name' => __('Border'),
		'type' => 'text',
	);
	return $fields;
}
add_filter('siteorigin_panels_row_style_fields', 'pagebuilder_panels_row_style_fields');

function pagebuilder_panels_panels_row_style_attributes($attr, $style) {
	$attr['style'] = '';
	if(!empty($style['padding'])) $attr['style'] .= 'padding: '.$style['padding'].'; ';
	if(!empty($style['margin-top'])) $attr['style'] .= 'margin-top: '.$style['margin-top'].'; ';
	if(!empty($style['margin-bottom'])) $attr['style'] .= 'margin-bottom: '.$style['margin-bottom'].'; ';
	if(!empty($style['background'])) $attr['style'] .= 'background: '.$style['background'].'; ';
	if(!empty($style['background-size'])) $attr['style'] .= 'background-size: '.$style['background-size'].'; ';
	if(!empty($style['border'])) $attr['style'] .= 'border: '.$style['border'].';';

	if(empty($attr['style'])) unset($attr['style']);
	return $attr;
}
add_filter('siteorigin_panels_row_style_attributes', 'pagebuilder_panels_panels_row_style_attributes', 10, 2);

/*STROSCH-CUSTOM-CODE*/
/*
function pagebuilder_panels_panels_row_classes($classes) {
	return $classes;
}
add_filter('strosch_panel_row_add_classes', 'pagebuilder_panels_panels_row_classes');
*/

?>