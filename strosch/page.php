<?php get_header(); ?>



	<div id="content">



		<div id="inner-content" class="container p borderlr">



			<div id="sidebar1" class="sidebar">

				<?php dynamic_sidebar('sidebar left'); ?>

				<?php // Banners left side

				if ( is_active_sidebar('Banners Left') ) : ?>

					<div class="banners-left-container">

						<?php dynamic_sidebar('Banners Left'); ?>

					</div>

				<?php endif; ?>

                <?php

				require_once 'Mobile_Detect.php';

				$detect = new Mobile_Detect;

				$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

				if($deviceType=='computer'){?>



				<?php if ( is_active_sidebar( 'sidebar_banner_left_sidebar' ) ) : ?>

				<?php dynamic_sidebar( 'sidebar_banner_left_sidebar' ); ?>

				<?php endif; ?>



				<?php if ( is_active_sidebar( 'partnerstorecode' ) ) : ?>

				<?php dynamic_sidebar( 'partnerstorecode' ); ?>

				<?php endif; ?>



				<?php } ?>

			</div>



			<main id="main" role="main">



				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">



						<?php

							the_content();

						?>



					</article>

<?php if ( function_exists( 'echo_ald_crp' ) ) echo_ald_crp(); ?>



				<?php endwhile; endif; ?>



				<?php // Partner Store Code

				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Partner Store Code')) : ?>

				<?php endif; ?>



			</main>



			<div id="sidebar2" class="sidebar">

				<?php dynamic_sidebar('sidebar right'); ?>

				<?php // Banners right side ?>

                <?php if($deviceType=='computer'){ ?>

                 <div class="banners-right-container">

				<?php if ( is_active_sidebar( 'sidebar_banner_right' ) ) : ?>

                <?php dynamic_sidebar( 'sidebar_banner_right' ); ?>

                <?php endif; ?>

                <?php if ( is_active_sidebar( 'partnerstorecoderight' ) ) : ?>

				<?php dynamic_sidebar( 'partnerstorecoderight' ); ?>

				<?php endif; ?>

                </div>

                <?php } ?>

			</div>



		</div>



	</div>



<?php get_footer(); ?>

