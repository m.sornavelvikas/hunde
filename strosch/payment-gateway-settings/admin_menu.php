<?php global $options; $options = get_option('payment_gateway_settings'); ?>
<form method="post" id="mainform" action="">

<table class="ciusan-plugin widefat" style="margin-top:50px;">
	<thead>
		<tr>
			<th scope="col">Page Settings</th>
			<th scope="col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="titledesc">Post Article Page</td>
			<td class="forminp">
            <?php $pages = get_pages(); ?>
            <select name="post_article_page"  style="width:250px;">
               <?php foreach ( $pages as $page ) { ?>
                <option value="<?php echo $page->ID; ?>" <?php if($page->ID==$options[post_article_page]){?>selected="selected"<?php } ?>><?php echo $page->post_title; ?></option>
               <?php } ?>
            </select>
			</td>
		</tr>
      
         <tr>
			<td class="titledesc">Post Management Page</td>
			<td class="forminp">
            <select name="post_management_page"  style="width:250px;">
               <?php foreach ( $pages as $page ) { ?>
                <option value="<?php echo $page->ID; ?>" <?php if($page->ID==$options[post_management_page]){?>selected="selected"<?php } ?>><?php echo $page->post_title; ?></option>
               <?php } ?>
            </select>
			</td>
		</tr>
        
        
        
           <tr>
			<td class="titledesc">IPN response Page</td>
			<td class="forminp">
            <select name="notify_url_page"  style="width:250px;">
               <?php foreach ( $pages as $page ) { ?>
                <option value="<?php echo $page->ID; ?>" <?php if($page->ID==$options[notify_url_page]){?>selected="selected"<?php } ?>><?php echo $page->post_title; ?></option>
               <?php } ?>
            </select>
			</td>
		</tr>
        

	</tbody>
</table>


<table class="ciusan-plugin widefat" style="margin-top:50px;">
	<thead>
		<tr>
			<th scope="col">Blog Subscription Price</th>
			<th scope="col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="titledesc">Price</td>
			<td class="forminp">
				<input name="blog_subscription_price" id="blog_subscription_price" style="width:250px;" value="<?php echo $options[blog_subscription_price]; ?>" type="number" class="required" placeholder="eg: &quot;20&quot;" step="any" required>
			</td>
		</tr>

	</tbody>
</table>

<!--Payment Getway settign start -->
<!----PayPal-->
<table class="ciusan-plugin widefat" style="margin-top:50px;">
	<thead>
		<tr>
			<th scope="col">PayPal Settings</th>
			<th scope="col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="titledesc">PayPal Email</td>
			<td class="forminp">
				<input name="paypal_email" id="paypal_email" style="width:250px;" value="<?php echo $options[paypal_email]; ?>" type="email" class="required" placeholder="eg: &quot;abhijit.codes@gmail.com&quot;">
			</td>
		</tr><tr>
			<td class="titledesc">Enable PayPal sandbox</td>
			<td class="forminp">
				<input type="checkbox" name="paypal_testmode" id="paypal_testmode" value="1" <?php if($options[paypal_testmode]==1){ echo 'checked'; }?>>&nbsp;<small class="description">PayPal sandbox can be used to test payments. Sign up for a developer account <a href="https://developer.paypal.com/">here</a>.</small>
			</td>
		</tr>
	</tbody>
</table>


<!------2Checkout---->
<table class="ciusan-plugin widefat" style="margin-top:50px;">
	<thead>
		<tr>
			<th scope="col">2Checkout Settings</th>
			<th scope="col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="titledesc">2Checkout account number</td>
			<td class="forminp">
				<input name="twocheckout_sid" id="twocheckout_sid" style="width:250px;" value="<?php echo $options[twocheckout_sid]; ?>" type="text" class="required" placeholder="">
			</td>
		</tr>
        <tr>
			<td class="titledesc">2Checkout Secret Word</td>
			<td class="forminp">
				<input name="twocheckout_secret_word" id="twocheckout_secret_word" style="width:250px;" value="<?php echo $options[twocheckout_secret_word]; ?>" type="text" class="required" placeholder="">
			</td>
		</tr>
        <tr>
			<td class="titledesc">2Checkout Sandbox</td>
			<td class="forminp">
				<input type="checkbox" name="twocheckout_sandbox" id="twocheckout_sandbox" value="1"  <?php if($options[twocheckout_sandbox]==1){ echo 'checked'; }?>>&nbsp;<small class="description">With the sandbox you can test 2Checkouts features against an exact clone of the production environment. Register for a sandbox account <a href="https://sandbox.2checkout.com/sandbox/signup" target="_blank">here</a>. </small>
			</td>
		</tr>
	</tbody>
</table>

<!------2Checkout---->
<table class="ciusan-plugin widefat" style="margin-top:50px;">
	<thead>
		<tr>
			<th scope="col">SOFORT Banking Settings</th>
			<th scope="col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="titledesc">Config Key</td>
			<td class="forminp">
				<input name="sofortgateway_configkey" id="sofortgateway_configkey" style="width:250px;" value="<?php echo $options[sofortgateway_configkey]; ?>" type="text" class="required" placeholder="">&nbsp;<small class="description">You need to enter your configkey or userid:projektid:apikey</small>
			</td>
		</tr>

	</tbody>
</table>
<!--Payment Getway settign end -->
<input type="hidden" name="action" value="update" />
<input type="hidden" name="page_options" value="<?php get_option($options) ?>" />
<p class="submit"><input type="submit" name="save" id="submit" class="button button-primary" value="Save Changes"/></p>
</form>
</div>

<div class="wrap"><hr /></div>