<?php 
namespace Sofort\SofortLib;
require('../../../../../wp-blog-header.php');
require __DIR__ . '/vendor/autoload.php';
$options = get_option('payment_gateway_settings'); 
ini_set('always_populate_raw_post_data',-1);

// You only can create a new configuration key by creating a new Gateway project in your account at sofort.com
$configkey = $options[sofortgateway_configkey];

// this class should be used as a callback function
$SofortLib_Notification = new Notification();
$TestNotification = $SofortLib_Notification->getNotification(file_get_contents('php://input'));
$SofortLibTransactionData = new TransactionData($configkey);

// If SofortLib_Notification returns a transaction_id:
//$SofortLibTransactionData->addTransaction('00907-01222-51ADD8C9-86C8');
$SofortLibTransactionData->addTransaction($TestNotification);

// By default without setter Api version 1.0 will be used due to backward compatibility, please set ApiVersion to
$SofortLibTransactionData->setApiVersion('2.0');
$SofortLibTransactionData->sendRequest();


if($SofortLibTransactionData->isError()) {
    echo $SofortLibTransactionData->getError();
} else {
    
	$getUserID = $SofortLibTransactionData->getUserVariable(0);
	$getStatus = $SofortLibTransactionData->getStatus();
	
	//$info = $getUserID.'-'.$getStatus;
	//mail('abhijit.codes@gmail.com','test',$info);
	
	
	 if($getStatus=='untraceable'){
	   $payment_status = 'Completed';
	 }else{
	   $payment_status = $getStatus;
	 }
	 
	 update_user_meta($getUserID, '_payment_status', $payment_status);
	  
	 if($payment_status=='Completed'){
		update_user_meta( $getUserID, '_payment_completed', true ); 
	 }
	
}

//$current_user = get_current_user_id();
//update_user_meta($current_user, '_payment_status','');
//update_user_meta( $current_user, '_payment_completed', false ); 