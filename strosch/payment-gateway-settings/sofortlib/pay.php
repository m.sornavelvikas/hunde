<?php
namespace Sofort\SofortLib;
require('../../../../../wp-blog-header.php');
require __DIR__ . '/vendor/autoload.php';
$current_user = get_current_user_id();
$options = get_option('payment_gateway_settings'); 

if($current_user>0 && !empty($current_user) ){

	$post_article_page_success = get_page_link($options[post_article_page]).'?st=1';
	$post_article_page_error = get_page_link($options[post_article_page]).'?st=2';
	$notify_url_page = get_site_url().'/wp-content/themes/strosch/payment-gateway-settings/sofortlib/ipn.php';
	
	$amount = $options[blog_subscription_price];
	// You only can create a new configuration key by creating a new Gateway project in your account at sofort.com
	$configkey = $options[sofortgateway_configkey];
	$Sofortueberweisung = new Sofortueberweisung($configkey);
	$Sofortueberweisung->setAmount($amount);
	$Sofortueberweisung->setUserVariable($current_user);
	$Sofortueberweisung->setCurrencyCode('EUR');
	$Sofortueberweisung->setReason('Pay For Blog Post');
	$Sofortueberweisung->setSuccessUrl($post_article_page_success, true); // i.e. http://my.shop/order/success
	$Sofortueberweisung->setAbortUrl($post_article_page_error);
	$Sofortueberweisung->setNotificationUrl($notify_url_page);

	$Sofortueberweisung->sendRequest();
	
	if($Sofortueberweisung->isError()) {
		// SOFORT-API didn't accept the data
		echo $Sofortueberweisung->getError();
	} else {
		// get unique transaction-ID useful for check payment status
		$transactionId = $Sofortueberweisung->getTransactionId();
		// buyer must be redirected to $paymentUrl else payment cannot be successfully completed!
		$paymentUrl = $Sofortueberweisung->getPaymentUrl();
		header('Location: '.$paymentUrl);
	}

}




